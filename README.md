# Maths

Author: Veronika Lapenok<br>
Mail: <veronika.lapenok@mail.ru><br>
License: LGPL 3.0

# 1. maths.py<br>
The console program in the Python3 programming language<br>
cyclically generates random arithmetic expressions<br> 
(+, -, *, /), checks the answer that is entered by the<br> 
user, and in case of an incorrect answer displays the<br> 
corresponding message on the screen along with the correct<br> 
answer. The exit from the cycle occurs by pressing the Enter<br> 
key. The program provides reference information on the number<br> 
of correct and incorrect answers and the percentage of solved<br> 
examples.

With the help of the arguments of the command line, the user<br>
can independently set the range of numbers to solve examples,<br>
as well as arithmetic signs. The user can transfer to the program<br> 
more than one arithmetic sign. For example - "+ + -", that is,<br> 
expressions with a "+" sign will drop about 66% more than<br>
expressions with the sign "-".

When dividing the result is rounded to hundredths.

Additional help is available with the command --help or -h.

# 2. gmaths.py<br>
The GUI program in the Python3 programming language<br>
cyclically generates random arithmetic expressions<br> 
(+, -, *, /), checks the answer that is entered by the<br> 
user, and in case of an incorrect answer displays the<br> 
corresponding message on the screen along with the correct<br> 
answer. The exit from the cycle occurs by pressing the Enter<br> 
key. The program provides reference information on the number<br> 
of correct and incorrect answers and the percentage of solved<br> 
examples.

With the help of the arguments of the command line, the user<br>
can independently set the range of numbers to solve examples,<br>
as well as arithmetic signs. The user can transfer to the program<br> 
more than one arithmetic sign. For example - "+ + -", that is,<br> 
expressions with a "+" sign will drop about 66% more than<br>
expressions with the sign "-".

When dividing the result is rounded to hundredths.

## Install<br>
For the program to work correctly, you may need to install the<br>
following modules:

sudo apt install python3-pip

- *Debian*<br>
    sudo apt install fonts-tlwg-purisa<br>
- *Arch Linux*<br>
    pacaur -S fonts-tlwg-purisa<br>

Additional help is available with the command --help or -h.



