#! /usr/bin/python3

import argparse, os, random, sys
from PIL import Image, ImageTk
from tkinter import Button, Entry, Frame, Label, LabelFrame, Tk
from tkinter.constants import LEFT, RIGHT

FONT = 'Purisa'
BACKGROUND_COLOR = "lavender"
SOURCE_PATH = os.path.abspath(os.path.dirname(__file__))
MINIMUM_VALUE = -100
MAXIMUM_VALUE = 100

# Get a random example
# Input:
#       minimum - minimum value of the random range (type: integer)
#       maximum - maximum value of the random range (type: integer)
# Output: example - string with an example (type: string)
def getExample(minimum, maximum):
    symbols = ['+', '-', '*']
    xValue = random.randint(minimum, maximum)
    yValue = random.randint(minimum, maximum)
    zValue = random.randint(minimum, maximum)
    
    example = str(xValue) + random.choice(symbols)

    if yValue < 0:
        example = example + "(" + str(yValue) + ")"
    else:
        example = example + str(yValue)

    example = example + random.choice(symbols)

    if zValue < 0:
        example = example + "(" + str(zValue) + ")"
    else:
        example = example + str(zValue)

    return example

# Exit from the program
# Input: void
# Output: void
def quit(event):
    tk.destroy()

tk = Tk()

topFrame = LabelFrame(tk, background=BACKGROUND_COLOR, text="Solve an example",
    font=(FONT, 15, 'bold'))
centerFrame = Frame(tk, background=BACKGROUND_COLOR)
bottomFrame = Frame(tk, background=BACKGROUND_COLOR)

exampleLabel = Label(topFrame, background=BACKGROUND_COLOR, font=(FONT, 17))
rightCounterLabel = Label(bottomFrame, background=BACKGROUND_COLOR, text="0 ",
    font=(FONT, 40), fg = 'green')
colonLabel = Label(bottomFrame, background=BACKGROUND_COLOR, text=": ",
    font=(FONT, 40))
wrongCounterLabel = Label(bottomFrame, background=BACKGROUND_COLOR, text="0 ",
    font=(FONT, 40), fg = 'red')
resultLabel = Label(centerFrame, background=BACKGROUND_COLOR, text="???",
    font=(FONT, 25))

answerEntry = Entry(topFrame, width=8, font=(FONT, 14))

answerButton = Button(topFrame, text="Check", font=FONT, height = 1, bg='light blue',
    activebackground='light sky blue', padx=5, pady=2)

emoji = Image.open(SOURCE_PATH + '/emoji/hello.png')
emoji = emoji.resize((150, 150), Image.ANTIALIAS)
emoji = ImageTk.PhotoImage(emoji)
panelEmoji = Label(bottomFrame, background=BACKGROUND_COLOR, image=emoji)

tk.title('GUI Maths')
tk.geometry('500x400')
tk.configure(background=BACKGROUND_COLOR)
tk.resizable(width=False, height=False)

exampleLabel.configure(text = getExample(MINIMUM_VALUE, MAXIMUM_VALUE))

panelEmoji.image = emoji

topFrame.pack(ipadx = 10, ipady = 10, padx = 10, pady = 10)
exampleLabel.pack(side = LEFT, padx=10, pady=10)
answerEntry.pack(side = LEFT)
answerButton.pack(side = LEFT, padx=10, pady=10)

centerFrame.pack()
rightCounterLabel.pack(side = LEFT)

bottomFrame.pack()
colonLabel.pack(side = LEFT)
wrongCounterLabel.pack(side = LEFT)
panelEmoji.pack(side = RIGHT)
resultLabel.pack()

tk.bind("<Escape>", quit)

tk.mainloop()