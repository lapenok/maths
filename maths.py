#! /usr/bin/python3

import argparse
import random
import sys

parser = argparse.ArgumentParser(
    description='Program for calculating arithmetic expressions. Expressions \
        are served randomly. To exit the program, click \'Enter\'',
    epilog='Veronika Lapenok <veronika.lapenok@mail.ru>')

parser.add_argument('--version', action='version', version='%(prog)s 1.0')
parser.add_argument('--usage', action='store_true', dest='isUsage', help='show this usage message and exit' )
parser.add_argument('--max', dest='maximum', type=int, default=100, help='set maximum limit of a numerical value \
    in a mathematical expression (type: int, default: 100)')
parser.add_argument('--min', dest='minimum', type=int,  default=-100, help='set minimum limit of a numerical value \
    in a mathematical expression (type: int, default: -100)')
parser.add_argument('--symb', dest='symbols', nargs='+', default=['+', '-', '/', '*'], help='set sign of the maths \
    expressions (default: + - / "*")')

results = parser.parse_args()

if results.isUsage == True:
    parser.print_usage()
    sys.exit(0)

if results.minimum > results.maximum:
    print('Error: the minimum value can not be greater than the maximum value')
    sys.exit(6)

for i in range(len(results.symbols)):
    if results.symbols[i] != '+' and results.symbols[i] != '-' and results.symbols[i] != '/' and results.symbols[i] != '*':
        print('Error: ' + str(i + 1) + ' value is incorrect in the "symb" option')
        sys.exit(7)

# random data generation
answer = 0
count = 0
rightAnswer = 0
percent = 0

while answer != "":
    symbol = random.choice(results.symbols)
    x_Value = random.randint(results.minimum, results.maximum)
    y_Value = random.randint(results.minimum, results.maximum)

    if symbol == "/" and y_Value == 0:
        continue

    # composing a math expression
    if symbol == "+":
        result = x_Value + y_Value
    elif symbol == "-":
        result = x_Value - y_Value
    elif symbol == "*":
        result = x_Value * y_Value
    elif symbol == "/":
        result = x_Value / y_Value
        result = round(result, 2)
   
    # output the generated expression to the screen
    while answer != "":
        if y_Value < 0:
            answer = input(str(x_Value) + " " + symbol + " " + "(" + str(y_Value) + ")" + " = ")
        else:
            answer = input(str(x_Value) + " " + symbol + " " + str(y_Value) + " = ")

        # verification of the user entered response to a numerical value / 
        try:
            answer = float(answer)
            break
        except ValueError:
            if answer != "":                
                print("Error: the data entered is not a number. Try again!") 

    # output of the result
    if result == answer: 
        print("Success!")
        rightAnswer += 1      
    elif answer != "":
        print("Error: answer is ", result)
    else:
        continue
    
    count += 1
    percent = round(rightAnswer * 100 / count, 2)
   
print(str(count) + " examples solved: " + str(rightAnswer) + "/" + str(count) + ": " + str(percent) + "%")           